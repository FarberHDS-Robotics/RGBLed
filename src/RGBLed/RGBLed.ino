#ifdef OUTPUT
const int redPin = 9;
const int greenPin = 10;
const int bluePin = 11;

void setup() {
  Serial.begin(9600);
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
}

void loop() {
  int red = random(256);
  int green = random(256);
  int blue = random(256);

  analogWrite(redPin, red);
  analogWrite(greenPin, green);
  analogWrite(bluePin, blue);

  Serial.println(String("Red = ") + red +
                String(",  Green = ") + green +
                String(",  Blue = ") + blue);

  delay(500);
}
#endif // !OUTPUT
